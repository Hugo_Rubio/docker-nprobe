FROM phusion/baseimage:latest

# Set correct environment variables.
ENV HOME /root

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# Update & Install from NTOP Package
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get -y -q install curl
RUN curl -s --remote-name http://apt-stable.ntop.org/16.04/all/apt-ntop-stable.deb
RUN sudo dpkg -i apt-ntop-stable.deb
RUN rm -rf apt-ntop-stable.deb

# Install nProbe
RUN apt-get update
RUN apt-get -y -q install nprobe

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Run & Obtain ID
RUN nprobe -v